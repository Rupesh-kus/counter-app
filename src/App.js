import React, { Component } from 'react'
import "./App.css"

export default class App extends Component {
  state = {
    count: 0
  }

  handleChange = () => {
    this.setState({
      count: this.state.count + 1
    })
  }
  handleDecrease = () => {
    this.setState({
      count: this.state.count - 1
    })
  }

  render() {
    return (
      <div className="App">
        <h1> Counter App</h1>
        <h2> Count: {this.state.count}</h2>
        <button onClick={() => { this.handleChange() }}>Increment</button>
        <button onClick={() => { this.handleDecrease() }}>Decrement</button>
      </div>
    )
  }
}
